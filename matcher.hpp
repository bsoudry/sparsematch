#ifndef MATCHER_H
#define MATCHER_H

#include "parse_mgf.hpp"
#include <vector>
#include <iostream>

class Matcher
{
public:
    Matcher(){};
    virtual ~Matcher(){};
    virtual void match(ParseMGF& parseMGF, std::vector<std::pair<int, int>>& matches) = 0;

};

#endif
