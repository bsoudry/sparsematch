#include "spectra_mgf.hpp"


SpectraMGF::SpectraMGF(){
    this->length = 0;
    return;
}

bool SpectraMGF::operator< (const SpectraMGF& other){
    return (mass_rounded < other.mass_rounded);
}


void SpectraMGF::filterPeaks(int N){
    if(length <= N){
        //No filtering needed, small enough
        return;
    }

    //Can be optimized later as needed
    
    //Remove smallest peak len-N times
    for(int i = 0; i < length-N; i++){    
        int min_index = -1;
        int min = INT_MAX;     
        for(int j = 0; j < peak_magnitudes_raw.size(); j++){
            if(peak_magnitudes_raw[j] < min){
                min = peak_magnitudes_raw[j];
                min_index = j;
            }
        }
        peak_magnitudes_raw.erase(peak_magnitudes_raw.begin() + min_index);
        peak_positions.erase(peak_positions.begin() + min_index);  
    } 
    length = N; 
}


void SpectraMGF::updateNormalizedPeaks(){
    float norm = 0; 
    for(int i = 0; i < peak_magnitudes_raw.size(); i++){
        norm += ((double) peak_magnitudes_raw[i])*((double) peak_magnitudes_raw[i]);
    }
    norm = std::sqrt(norm);
    
    //Normalize vector:
    peak_magnitudes_normalized.resize(peak_magnitudes_raw.size());

    for(int i = 0; i < peak_magnitudes_raw.size(); i++){
        peak_magnitudes_normalized[i] = peak_magnitudes_raw[i] / norm;
    }

}

void SpectraMGF::roundPositions(double min, double interval_size){
    //Get discrete, rounded values for peak positions.
    peak_positions_rounded.resize(peak_positions.size());

    for(int i = 0; i < peak_positions.size(); i++){
        peak_positions_rounded[i] = (int) std::lround((peak_positions[i]-min)/interval_size);
    }
}

void SpectraMGF::roundMass(double min, double interval_size){
    //Get discrete, rounded value for mass
    mass_rounded = (int) std::lround((mass - min) / interval_size);
}
