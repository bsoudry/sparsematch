#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include "parse_mgf.hpp"
#include "matcher.hpp"
#include "sparse_matcher.hpp"
#include "bruteforce_matcher.hpp"

void testMatcher(Matcher& matcher, ParseMGF parseMGF, int N){
        //Set dataset size
        std::cout << "============================" << std::endl;
        std::cout << "Testing N = " << N << std::endl;
        parseMGF.setNumSpectra(N);

    	std::cout << "Parsed " << parseMGF.spectras.size() << " Spectras" << std::endl;
        //Run matcher
        std::vector<std::pair<int, int>> matches;
        std::cout << "Start clock" << std::endl;
        auto start = std::chrono::high_resolution_clock::now();     
       
        matcher.match(parseMGF, matches); 
        
        auto stop = std::chrono::high_resolution_clock::now(); 
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start); 

        std::cout << "Found " << matches.size() << " matches in "
                  << duration.count() / 1000.0 << " seconds" << std::endl; 
}

void testParams(double res, int filter_peaks){
    ParseMGF parseMGF("../list4.txt", true, filter_peaks, res, 1.0, -1, -1);

    //ParseMGF parseMGF("./data", false, filter_peaks, res, 1.0, 25000, 10000000);
    std::cout << parseMGF.total_num_peaks << std::endl;

    double sparsity = ((double) parseMGF.total_num_peaks) /  (((double)parseMGF.max_pos_index) * (double)(parseMGF.all_spectras.size()) );
    double avg_peaks = ((double) parseMGF.total_num_peaks) /  (double)(parseMGF.all_spectras.size());
    std::cout << "Sparsity Rate: " << sparsity << std::endl;
    std::cout << "Avg peaks per spectra: " << avg_peaks << std::endl;
 
    std::cout << "max_pos_i: " << parseMGF.max_pos_index << " max_pos: " << parseMGF.max_pos << " min_pos: " << parseMGF.min_pos << std::endl;

    std::cout << "max_mass_i: " << parseMGF.max_mass_index << " max_mass: " << parseMGF.max_mass << " min_mass: " << parseMGF.min_mass << std::endl;

    SparseMatcher sparseMatcher(1.0); 

    BruteforceMatcher bruteforceMatcher(1.0);
 
    std::cout << "====================" << std::endl;
    std::cout << "Sparse Matcher: " << std::endl;
    testMatcher(sparseMatcher, parseMGF, -1); //Just match all spectras.

    

    /*std::vector<int> N = {10000, 100000, 1000000};
    for(int i = 0; i < N.size(); i++){
	std::cout << "====================" << std::endl;
	std::cout << "Sparse Matcher: " << std::endl;
	testMatcher(sparseMatcher, parseMGF, N[i]);
	
	std::cout << "====================" << std::endl;
	std::cout << "Bruteforce Matcher: " << std::endl;
	testMatcher(bruteforceMatcher, parseMGF, N[i]);
    }*/
}


int main() {
    testParams(0.01, 5);
    /*std::vector<int> filter = {5, 10, 20, 50, 100};
    for(int i = 0; i < filter.size(); i++){
	std::cout << "======================================" << std::endl;
	std::cout << "Filter test: " << filter[i] << std::endl;
	std::cout << "======================================" << std::endl;
        testParams(0.01, filter[i]);
    }*/

    /*std::vector<double> res = {0.01, 0.02, 0.05, 0.1};
    for(int i = 0; i < res.size(); i++){
       	std::cout << "======================================" << std::endl;
	std::cout << "Resolution test: " << res[i] << std::endl;
	std::cout << "======================================" << std::endl;
	testParams(res[i], -1);
    }*/
}


