#include "parse_mgf.hpp"

ParseMGF::ParseMGF(const std::string& spectra_file, 
	bool is_list_of_files,
	int filter_peaks, 
	double pos_bin_size,
	double mass_bin_size, 
	int max_files, 
	int max_spectra) 
: filter_peaks(filter_peaks), pos_bin_size(pos_bin_size), mass_bin_size(mass_bin_size) {
    curr_id = 0;
    total_num_peaks = 0;
    
    max_pos = std::numeric_limits<double>::min();
    min_pos = std::numeric_limits<double>::max();
    
    max_mass = std::numeric_limits<double>::min();
    min_mass = std::numeric_limits<double>::max();

    if(!is_list_of_files){
	//Spectra file is a directory, parse each file in the directory
	int files_parsed = 0;
	for (const auto & entry : std::experimental::filesystem::directory_iterator(spectra_file)){
	    if(getFileExtension(entry.path()) == "mgf"){
		std::cout << "Parsing " << entry.path() << std::endl;
		int parse_status = parseFile(entry.path());
		if(parse_status < 0){
		    std::cout << "Error parsing " << entry.path() << std::endl;
		    continue;
		}
		files_parsed++;
		if(files_parsed >= max_files && max_files != -1){
		    break;
		}
	    }
	}
    } else {
	//Spectra file is a list of files, parse all files in the list.
	std::string path;
    	std::ifstream file (spectra_file);
	int files_parsed = 0;
    	
	if(file.is_open()){
            while(std::getline(file, path))
            {
		std::cout << "Parsing " << path << std::endl;
		int parse_status = parseFile(path);
		if(parse_status < 0){
		    std::cout << "Error parsing " << path << std::endl;
		    continue;
		}
		files_parsed++;
		if(files_parsed >= max_files && max_files != -1){
		    break;
		}				
            }
    	}
    }

    //Shuffle all spectra
    std::default_random_engine rand();
    std::random_shuffle(all_spectras.begin(), all_spectras.end());
    
    setNumSpectra(max_spectra);
}


//Sets the number of spectra in the currently active set (spectras), 
//and does all processing on that set including filtering peaks and normalizing
void ParseMGF::setNumSpectra(int max_spectra){
    
    //Sample spectra so that we have no more than max_spectra
    if(max_spectra == -1){
        //No max given, just use all parsed spectras
	max_spectra = all_spectras.size(); 
    }
    else if(max_spectra > all_spectras.size()){
        std::cout << "Warning: max_spectra higher than spectras parsed" << std::endl;
        max_spectra = all_spectras.size(); 
    }
    spectras.resize(max_spectra);
    for(int i = 0; i < max_spectra; i++){
        spectras[i] = all_spectras[i]; 
    }

    //Filter peaks if necessary and calculate normalized peak magnitudes 
    //and round peak positions, spectra mass to discrete values
    if(filter_peaks != -1){
        for(int i = 0; i < spectras.size(); i++){
            spectras[i].filterPeaks(filter_peaks);
        }
    }
    std::cout << "filtered peaks" << std::endl;
    for(int i = 0; i < spectras.size(); i++){
        spectras[i].updateNormalizedPeaks();
        spectras[i].roundPositions(min_pos, pos_bin_size);
        spectras[i].roundMass(min_mass, mass_bin_size);
	
    	total_num_peaks += spectras[i].length;    
    }
    std::cout << "round + normalize" << std::endl;

    //Calculate maximum position / mass indexes
    max_pos_index = (int) std::lround((max_pos - min_pos) / pos_bin_size);
    max_mass_index = (int) std::lround((max_mass - min_mass) / mass_bin_size);

    std::cout << "calculated max index: " << std::endl;
    
    //Sort all the spectra by Mass
    std::sort(spectras.begin(), spectras.end());
}


int ParseMGF::parseFile(const std::string& spectra_file){
    std::string line;
    std::ifstream file (spectra_file);
  
    if(file.is_open())
    {
        while(std::getline(file, line))
        {
            if(line.compare("BEGIN IONS") == 0){
                //Parse each spectra
                SpectraMGF spectra;
                int parse_status = parseSpectra(file, spectra);
                
                if(parse_status < 0){
                    file.close();
                    return parse_status;
                }
            }
        }
        
        file.close();
    } else {
        return -1;
    }
    return 0;
}

int ParseMGF::parseSpectra(std::ifstream& file, SpectraMGF& spectra){

    std::string line;
    if(std::getline(file, line) && (line.substr(0,6).compare("TITLE=") == 0)){
        spectra.title = line.substr(6);
    } else {
        return -1;
    }
    
    if(std::getline(file, line) && (line.substr(0,8).compare("PEPMASS=") == 0)){
        spectra.mass = std::stod(line.substr(8));
        if(spectra.mass > max_mass){
            max_mass = spectra.mass;
        }
        if(spectra.mass < min_mass){
            min_mass = spectra.mass;
        }
    } else {
        return -1;
    }

    while(std::getline(file, line) && line.compare("END IONS") != 0)
    {
         if(line.length() == 0 || !std::isdigit(line[0])){
             continue;
	 }
	 if(line.find_last_of(" ") != std::string::npos){
            double pos = std::stod(line.substr(0,line.find_last_of(" ")));           
            int mag = std::stoi(line.substr(line.find_last_of(" ")+1)); 
            
            spectra.peak_positions.push_back(pos);
            spectra.peak_magnitudes_raw.push_back(mag);

            if(pos < min_pos){
                min_pos = pos;
            }
            if(pos > max_pos){
                max_pos = pos;
            }
         } else {
            return -1;
         }
    }
    spectra.length = spectra.peak_positions.size();

    spectra.id = curr_id;
    curr_id++;
    
    this->all_spectras.push_back(spectra); 
    return 0;
}

std::string ParseMGF::getFileExtension(const std::string& file_name)
{
    if(file_name.find_last_of(".") != std::string::npos)
        return file_name.substr(file_name.find_last_of(".")+1);
    return "";
}

