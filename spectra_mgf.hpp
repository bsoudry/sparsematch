#ifndef SPECTRA_MGF_H
#define SPECTRA_MGF_H


#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <climits>
#include <cmath> 

class SpectraMGF 
{
    public:
        std::string title;
        int id;

        std::vector<double> peak_positions;
        std::vector<int> peak_positions_rounded;

        std::vector<int> peak_magnitudes_raw;
        std::vector<double> peak_magnitudes_normalized;
        
        double mass;
        int mass_rounded;
    
        int length;
       

        SpectraMGF();
        void filterPeaks(int N);
        void updateNormalizedPeaks();

        void roundPositions(double min, double interval_size);
        void roundMass(double min, double interval_size);
        
        bool operator< (const SpectraMGF& other);
 };


#endif
