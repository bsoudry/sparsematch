#ifndef PARSE_MGF_H
#define PARSE_MGF_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <experimental/filesystem>
#include <limits>
#include <algorithm>
#include "spectra_mgf.hpp"
#include <random>

class ParseMGF 
{
    public:
    
    std::vector<SpectraMGF> spectras; //The subset of shuffled spectras currently for use, is size set by max_spectra argument
         
    std::vector<SpectraMGF> all_spectras;


    ParseMGF(const std::string& spectra_directory, bool is_list_of_files, int filter_peaks, double pos_bin_size, double mass_bin_size, int max_files, int max_spectra);
    
    void setNumSpectra(int max_spectra);
        
    double pos_bin_size;
    double mass_bin_size;
    int filter_peaks;

    double min_pos;
    double max_pos;

    double max_mass;
    double min_mass;
 
    int max_pos_index;

    int max_mass_index;

    long total_num_peaks;    

    private:
    int curr_id;    
    
    int parseFile(const std::string& spectra_file);
    int parseSpectra(std::ifstream& file, SpectraMGF& spectra);

    std::string getFileExtension(const std::string& file_name);

};

#endif
