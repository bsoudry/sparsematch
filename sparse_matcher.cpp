#include "sparse_matcher.hpp"

SparseMatcher::SparseMatcher(double match_threshold){
    this->match_threshold = match_threshold;
    return;
}

void SparseMatcher::match(ParseMGF& parseMGF, std::vector<std::pair<int,int>>& matches) {
    auto construct_start = std::chrono::high_resolution_clock::now();    
    constructMatrix(parseMGF);
    auto construct_stop = std::chrono::high_resolution_clock::now(); 
    auto construct_duration = std::chrono::duration_cast<std::chrono::milliseconds>
	(construct_stop - construct_start); 

    std::cout << "	Matrix construction step took " << construct_duration.count() / 1000.0 
	      << " seconds" << std::endl; 
 

    auto match_start = std::chrono::high_resolution_clock::now();    
    for(int i = 0; i < parseMGF.spectras.size(); i++){
        matchSpectra(i, parseMGF, matches);
    }
    
    auto match_stop = std::chrono::high_resolution_clock::now();    
    auto match_duration = std::chrono::duration_cast<std::chrono::milliseconds>
	(match_stop - match_start); 

    std::cout << "	Sparse Matching took " << match_duration.count() / 1000.0 
	      << " seconds" << std::endl;
}

void SparseMatcher::matchSpectra(int index, ParseMGF& parseMGF, std::vector<std::pair<int,int>>& matches) {
    SpectraMGF curr_spectra = parseMGF.spectras[index];
    
    //Get the matrix corresponding with the same mass curr_spectra - @TODO add mass tolerance
    std::vector<std::unordered_map<int, double>>& curr_matrix = matrix[curr_spectra.mass_rounded];

    //Calculate dot products using the sparse matrix multiply
    std::unordered_map<int, double> dot_products; //spectra id -> dot_prod

    for(int i = 0; i < curr_spectra.length; i++){
        int curr_bin = curr_spectra.peak_positions_rounded[i];
        double curr_magnitude = curr_spectra.peak_magnitudes_normalized[i];
        
        for (std::pair<int, double> elem : curr_matrix[curr_bin])
        {
            double result = curr_magnitude * elem.second;
            if(dot_products.find(elem.first) == dot_products.end()){
                dot_products[elem.first] = result;
            } else {
                dot_products[elem.first] += result;          
            }
        }
    }
    
    //Collect pairs with high dot products as matches
    for (std::pair<int, double> elem : dot_products)
    {
        if(elem.second > match_threshold){
            //std::cout << "Found match: " << curr_spectra.id << ", " << elem.first << " with dot prod: " << elem.second << std::endl;
            matches.push_back(std::make_pair(curr_spectra.id, elem.first));
        }
    }
    return;
}

void SparseMatcher::constructMatrix(ParseMGF& parseMGF){
    /*  Data is organized in the matrix as follows:
     *      If a peptide with mass m has a peak at position p and the peptide id is i, 
     *      then matrix[m][p][i] stores the magnitude of that peak as a double.
     *
     *      Note, the mass m and position p were discretized and offset to start at 0 in parseMGF
     */
    std::cout << "constructing matrix " << std::endl;
    matrix.resize(parseMGF.max_mass_index + 1);

    for(int m = 0; m < matrix.size(); m++){
        matrix[m].resize(parseMGF.max_pos_index + 1);
        for(int p = 0; p < matrix[m].size(); p++){
            matrix[m][p] = std::unordered_map<int, double>();
        }
    }

    std::cout << "finished initializing " << std::endl;
    int mgf_index = 0;    

    for(int m = 0; m < matrix.size(); m++){
        while(mgf_index < parseMGF.spectras.size() && parseMGF.spectras[mgf_index].mass_rounded == m){
            //Add spectra to matrix
            SpectraMGF& curr_spectra = parseMGF.spectras[mgf_index];
            
            for(int i = 0; i < curr_spectra.length; i++){
                int pos = curr_spectra.peak_positions_rounded[i];
                matrix[m][pos].insert(std::make_pair(curr_spectra.id, curr_spectra.peak_magnitudes_normalized[i]));
            }
            mgf_index++;
            
        }         
        assert(mgf_index >= parseMGF.spectras.size() || m <= parseMGF.spectras[mgf_index].mass_rounded);
    }
    
    std::cout << "finished constructing matrix " << std::endl;
} 

